#include <string.h>
#include <stdio.h>
#include <time.h>
#include <stdint.h>
#include <ctype.h>
#include <unistd.h>

#include "SHA3api_ref.h"
#include "generator.h"
#include "bitcount.h"

#define likely(x)    __builtin_expect (!!(x), 1)
#define unlikely(x)  __builtin_expect (!!(x), 0)

void ShowBytes(uint_t cnt,const u08b_t *b)
{ /* formatted output of byte array */
	uint_t i;

	for (i=0;i < cnt;i++)
	{
		if (i %16 ==  0) printf("    ");
		else if (i % 4 == 0) printf(" ");
		printf(" %02X",b[i]);
		if (i %16 == 15 || i==cnt-1) printf("\n");
	}
}

void hex_to_bytes(const char* hex, BitSequence* bytes)
{
	size_t len = strlen(hex);

	for (int i = 0; i < (len / 2); i++) {
		sscanf(hex + 2*i, "%02x", (int*)&bytes[i]);
	}
}

#define BITS 1024
#define BYTES 1024/8

int main()
{
	generator_init();
	bitcount_init();

	pid_t pid = getpid();
	char fname[255];
	sprintf(fname, "%i_guess.txt", (int)pid);
	FILE* fp = fopen(fname, "w");    

	const char* target_string = "5b4da95f5fa08280fc9879df44f418c8f9f12ba424b7757de02bbdfbae0d4c4fdf9317c80cc5fe04c6429073466cf29706b8c25999ddd2f6540d4475cc977b87f4757be023f19b8f4035d7722886b78869826de916a79cf9c94cc79cd4347d24b567aa3e2390a573a373a48a5e676640c79cc70197e1c5e7f902fb53ca1858b6";
	BitSequence target[255];

	BitSequence output[BYTES];
#ifdef BENCHMARK
	long counter = 0;
	clock_t lastMeasurement = clock();
#endif

	hex_to_bytes(target_string, target);

	while (1)
	{
		int length = generator_random();
		Hash(BITS, (const BitSequence*)encoded_bytes, length*8, output);
		unsigned int diff = bitcount_difference(target, output, 128);

		if (unlikely(diff <= 407))
		{
			printf("%s: %i (%s)\n", bytes, diff, encoded_bytes);
			fprintf(fp, "%s: %i (%s)\n", bytes, diff, encoded_bytes);
			fflush(fp);
		}

#ifdef BENCHMARK
		counter++;

		if (unlikely(counter%1000 == 0 && lastMeasurement + CLOCKS_PER_SEC < clock()))
		{
			printf("%ld\n", counter);
			counter = 0;
			lastMeasurement = clock();
		}
#endif
	}

	return 0;
}