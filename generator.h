#ifndef GENERATOR_H
#define GENERATOR_H

#include "SHA3api_ref.h"

extern char encoded_bytes[255];
extern char bytes[255];

void generator_init();
int generator_random();

#endif