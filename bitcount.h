#ifndef BITCOUNT_H
#define BITCOUNT_H

#include "SHA3api_ref.h"

void bitcount_init();
unsigned int bitcount_difference(BitSequence* target, BitSequence* hash, unsigned int length);

#endif