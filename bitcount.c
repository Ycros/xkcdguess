#include "bitcount.h"

static unsigned int bitcounts[65536];

void bitcount_init()
{
	int position1 = -1;
	int position2 = -1;
	//
	// Loop through all the elements and assign them.
	//
	for (int i = 1; i < 65536; i++, position1++)
	{
		//
		// Adjust the positions we read from.
		//
		if (position1 == position2)
		{
			position1 = 0;
			position2 = i;
		}
		bitcounts[i] = bitcounts[position1] + 1;
	}	
}

unsigned int bitcount_count(unsigned int value)
{
	return bitcounts[value & 65535] + bitcounts[(value >> 16) & 65535];
}

unsigned int bitcount_count_2(unsigned int i)
{
    i = i - ((i >> 1) & 0x55555555);
    i = (i & 0x33333333) + ((i >> 2) & 0x33333333);
    return (((i + (i >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
}

unsigned int bitcount_difference(BitSequence* target, BitSequence* hash, unsigned int length)
{
	unsigned int result = 0;

	for (int i = 0; i < length; i++)
	{
		unsigned int difference = target[i] ^ hash[i];
		unsigned int count = bitcount_count(difference);
		result += count;
	}

	return result;
}