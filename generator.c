#include "generator.h"
#ifdef USE_MT
#include "mersenne-twister.h"
#endif

#include <strings.h>
#include <sys/time.h>
#include <ctype.h>
#include <unistd.h>

#ifndef USE_MT
#include <stdlib.h>
#endif

char bytes[255];
char encoded_bytes[255];
static int bytes_length;
static int bytes_choices[255];
static char choices[255];
static int choices_length;

void fill_choices()
{
	for (int i = 0; i < 255; ++i)
	{
		if ((i >= 32 && i <= 126))
		{
			choices[choices_length] = i;
			choices_length++;
		}
	}
}

void generator_init()
{
	bzero(choices, 255);
	bzero(bytes, 255);
	bzero(encoded_bytes, 255);
	bzero(bytes_choices, sizeof(int)*255);
	choices_length = 0;
	bytes_length = 0;
	fill_choices();
	
	struct timeval tv;
	gettimeofday(&tv, NULL);
#ifndef USE_MT
	//srandom(tv.tv_sec);
	srandom(tv.tv_sec ^ tv.tv_usec - getpid());
#else
	srand(tv.tv_sec ^ tv.tv_usec - getpid());
#endif
}

static char hex[] = "0123456789ABCDEF";

/* Converts an integer value to its hex character*/
static inline char to_hex(char code) {
  return hex[code & 15];
}

int generator_random()
{
#ifndef USE_MT
	int length = (random() % 32) + 5;
#else
	int length = (rand() % 32) + 5;
#endif
	int bytes_length = 0;

	for (int i = 0; i < length; ++i)
	{
#ifndef USE_MT
		int choice_index = random() % choices_length;
#else
		int choice_index = rand() % choices_length;
#endif
		char choice = choices[choice_index];
		bytes[bytes_length++] = choice;

		if (!(isalnum(choice) || choice == '-' || choice == '_' || choice == '.' || choice == '*'))
		{
			if (choice == ' ')
			{
				choice = '+';
			}
			else
			{ 
				length += 2;
				encoded_bytes[i++] = '%';
				encoded_bytes[i++] = to_hex(choice >> 4);
				encoded_bytes[i] = to_hex(choice & 15);
				continue;
			}
		}

		encoded_bytes[i] = choice;
	}

	encoded_bytes[length] = 0;
	bytes[bytes_length++] = 0;

	return length;
}

