TARGET = guess
LIBS = -lm
CC = gcc
CXX = g++
LDFLAGS = -m64
CFLAGS = -Wall -std=gnu99
#CFLAGS += -g
CFLAGS += -O3 -march=native -ffast-math -fomit-frame-pointer -funroll-loops
#CFLAGS += -DBENCHMARK=1
CFLAGS += -DUSE_MT=1
CFLAGS += -m64

.PHONY: default all clean

default: $(TARGET)
all: default

OBJECTS = $(patsubst %.c, %.o, $(wildcard *.c)) $(patsubst %.cpp, %.o, $(wildcard *.cpp))
HEADERS = $(wildcard *.h)

%.o: %.cpp $(HEADERS)
	$(CXX) $(CFLAGS) -c $< -o $@

%.o: %.c $(HEADERS)
	$(CC) $(CFLAGS) -c $< -o $@

.PRECIOUS: $(TARGET) $(OBJECTS)

$(TARGET): $(OBJECTS)
	$(CC) $(OBJECTS) $(LDFLAGS) -Wall $(LIBS) -o $@

clean:
	-rm -f *.o
	-rm -f $(TARGET)
